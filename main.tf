terraform {

  required_version = ">=0.12"

  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  features {}

  subscription_id   = "1528db11-b42f-4e54-b34f-607397ef0f73"
  tenant_id         = "813f4081-ad7f-441d-9aff-4af7a783a6d7"
  client_id         = "8584655f-0c40-4df7-bcae-9812586135dd"
  client_secret     = "$pecccccccccccccccc"
}

resource "azurerm_resource_group" "azure_rg" {
  name     =  var.rgname
  location =  var.location
}

# Virtual Network
resource "azurerm_virtual_network" "vnet" {
    name                 = var.vnet_name
    address_space        = var.address_space
    location             = var.location
    resource_group_name  = var.rgname
}
# Subnet for virtual machine
resource "azurerm_subnet" "vmsubnet" {
  name                  =  var.subnet_name
  address_prefixes        =  var.address_prefix
  virtual_network_name  =  var.vnet_name
  resource_group_name   =  var.rgname
}

# Add a Public IP address
resource "azurerm_public_ip" "vmip" {
    count                  = var.numbercount
    name                   = "vm-ip-${count.index}"
    resource_group_name    =  var.rgname
    allocation_method      = "Static"
    location               = var.location
}

# Add a Network security group
resource "azurerm_network_security_group" "nsgname" {
    name                   = "vm-nsg"
    location               = var.location
    resource_group_name    =  var.rgname

    security_rule {
        name                       = "PORT_SSH"
        priority                   = 101
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
  }
}
#Associate NSG with  subnet
resource "azurerm_subnet_network_security_group_association" "nsgsubnet" {
    subnet_id                    = azurerm_subnet.vmsubnet.id
    network_security_group_id    = azurerm_network_security_group.nsgname.id
}

# NIC with Public IP Address
resource "azurerm_network_interface" "terranic" {
    count                  = var.numbercount
    name                   = "vm-nic-${count.index}"
    location               = var.location
    resource_group_name    =  var.rgname

    ip_configuration {
        name                          = "external"
        subnet_id                     = azurerm_subnet.vmsubnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = element(azurerm_public_ip.vmip.*.id, count.index)
  }

}
#Data Disk for Virtual Machine
resource "azurerm_managed_disk" "datadisk" {
 count                = var.numbercount
 name                 = "datadisk_existing_${count.index}"
 location             = var.location
 resource_group_name  = var.rgname
 storage_account_type = "Standard_LRS"
 create_option        = "Empty"
 disk_size_gb         = "50"
}

#Aure Virtual machine
resource "azurerm_virtual_machine" "terravm" {
    name                  = "${format("node-%01d", count.index + 1)}"
    location              = var.location
    resource_group_name   = var.rgname
    count = var.numbercount
    network_interface_ids = [element(azurerm_network_interface.terranic.*.id, count.index)]
    vm_size               = "Standard_B1ls"
    delete_os_disk_on_termination = true
    delete_data_disks_on_termination = true


storage_os_disk {
    name                 = "osdisk-${count.index}"
    caching              = "ReadWrite"
    create_option        = "FromImage"
    managed_disk_type    = "Premium_LRS"
    disk_size_gb         = "30"
  }

 storage_data_disk {
   name              = element(azurerm_managed_disk.datadisk.*.name, count.index)
   managed_disk_id   = element(azurerm_managed_disk.datadisk.*.id, count.index)
   create_option     = "Attach"
   lun               = 1
   disk_size_gb      = element(azurerm_managed_disk.datadisk.*.disk_size_gb, count.index)
 }

   storage_image_reference {
    publisher       = "Canonical"
    offer           = "UbuntuServer"
    sku             = "16.04-LTS"
    version         = "latest"
  }
  os_profile {
        computer_name = "hostname"
        admin_username = "intouchwifi"
    }

    os_profile_linux_config {
      disable_password_authentication = true

        ssh_keys {
        path     = "/home/intouchwifi/.ssh/authorized_keys"
        key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCV+2KtV27/TOFApdtbJ3NsrG4IhqrgF/ys0IjggSmm3scwaXacpgc3hfCbvANucBtLRTQePn2ijddWR9OWFfconABH5H6ylsXsanO7ydCyhwgO0Tw6t3wBMRbzhRpBRys0IR8TrbQGVidP4695moTLC/xJ5mdOQ/g9Vv5vGA8WkeagFZgLljED6jfYadCq1UQrBmuDrHkzLvQRvW9UEcGxMCLPmjCVOH7FwUuojFkybjZOnXSqQ6oVQ1Gcdy684pImIz6YyNb5RV2lUbnZL4Xw71Vzq/1WYhzkdzCDDHSejlbSe15pumz1uQwO5YtmQy4tSeSmn1uMoC53RT4ATY8P intouchwifi@cc-acb322d4-cdcf5f697-8wtjh"
      }
    }

   connection {
        host = azurerm_public_ip.admingwip.id
        user = "techies"
        type = "ssh"
        private_key = "${file("./id_rsa")}"
        timeout = "1m"
        agent = true
  }
}
